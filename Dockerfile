FROM debian:bullseye

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
ENV LD_RUN_PATH=/usr/local/lib

WORKDIR /opt/taler-ci

ADD install*.sh ./
ADD test.sh ./

RUN ./install.sh
